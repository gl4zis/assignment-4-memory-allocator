#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define MIN_BLOCK_CAPACITY 24
#define MAP_ANONYMOUS 0x20

static struct block_header* block_from_content(void* content) {
    return (struct block_header*) (content - offsetof(struct block_header, contents));
}

static char* malloc_test(void) {
    uint8_t* content = _malloc(0);
    struct block_header* block = block_from_content(content);

    assert(block->capacity.bytes == MIN_BLOCK_CAPACITY);
    assert(block->next);
    assert(block == HEAP_START);
    assert(!block->is_free);

    return "MALLOC OK";
}

static char* block_free(void) {
    void* content1 = _malloc(0);
    void* content2 = _malloc(0);

    _free(content2);

    struct block_header* block1 = block_from_content(content1);
    struct block_header* block2 = block_from_content(content2);

    assert(block1->next == block2);
    assert(block2->is_free);

    return "FREE OK";
}

static char* two_block_free(void) {
    void* content1 = _malloc(0);
    void* content2 = _malloc(0);
    void* content3 = _malloc(0);

    _free(content3);
    _free(content2);

    struct block_header* block1 = block_from_content(content1);
    struct block_header* block2 = block_from_content(content2);
    size_t block1_bytes = size_from_capacity((block_capacity) {MIN_BLOCK_CAPACITY}).bytes;
    size_t block2_bytes = size_from_capacity(block2->capacity).bytes;

    assert(block1->next == block2);
    assert(block2->is_free);
    assert(!block2->next);
    assert(block1_bytes + block2_bytes == REGION_MIN_SIZE);

    return "TWO FREE OK";
}

#define ALMOST_REGION_CAP 8000
static char* mem_extend(void) {
    _malloc(ALMOST_REGION_CAP);
    struct block_header* extending = block_from_content(_malloc(ALMOST_REGION_CAP));

    struct block_header* start = HEAP_START;

    assert(start->next == extending);
    assert(start->capacity.bytes == ALMOST_REGION_CAP);
    assert(extending->capacity.bytes == ALMOST_REGION_CAP);
    assert(extending->next);
    assert((void*)start + offsetof(struct block_header, contents) + ALMOST_REGION_CAP == (void*) extending);

    return "MEMORY EXTEND OK";
}

static char* extend_mem_in_other_place(void) {
    void* hole = mmap(HEAP_START + REGION_MIN_SIZE, 100, 0, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header* extending = block_from_content(_malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents)));
    munmap(hole, 100);

    struct block_header* start = HEAP_START;

    assert(start->next == extending);
    assert(!extending->next);
    assert((void*) start + offsetof(struct block_header, contents) + start->capacity.bytes != extending);

    return "MEMORY EXTEND TWO OK";
}

static void do_test(char* (*test)(void), int number) {
    printf("Run test number %d\n", number);
    heap_init(0);
    char* message = test();
    heap_term();
    printf("%d: %s\n\n", number, message);
}

int main() {
    do_test(malloc_test, 1);
    do_test(block_free, 2);
    do_test(two_block_free, 3);
    do_test(mem_extend, 4);
    do_test(extend_mem_in_other_place, 5);

    return 0;
}
